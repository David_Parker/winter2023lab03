import java.util.Random;
public class Dolphin {
	public int swimmingSpeed;
	public double weight;
	public boolean longSnout;
	
	public void jumpOutOfWater() {
		double jumpHeight = this.swimmingSpeed - (this.weight*0.05);
		System.out.println("The dolphin jumped " + jumpHeight + "cm into the air!");
	}
	
	public void catchFood() {
		if(this.longSnout) {
			System.out.println("The dolphin caught a crab!");
		}
		else {
		System.out.println("The dolphin caught a fish!");
		}
	}
}