import java.util.Scanner;
public class NationalPark {
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		Dolphin[] pod = new Dolphin[4];
		
		for(int i=0; i<pod.length; i++) {
			pod[i] = new Dolphin();
			System.out.println("What is the weight of the dolphin in kg?");
			pod[i].weight = reader.nextDouble();
			System.out.println("What is the swimming speed of the dolphin in km/h?");
			pod[i].swimmingSpeed = reader.nextInt();
			System.out.println("Does the dolphin have a long snout?(true or false)");
			pod[i].longSnout = reader.nextBoolean();
		}
		
		System.out.println("");
		
		System.out.println("This is the description of the last dolphin:");
		System.out.println(pod[pod.length-1].weight + "kg");
		System.out.println(pod[pod.length-1].swimmingSpeed + "km/h");
		System.out.println(pod[pod.length-1].longSnout);
		
		System.out.println("");
		
		System.out.println("The first dolphin performed these actions:");
		pod[0].jumpOutOfWater();
		pod[0].catchFood();
		
	}
}